/* Translation unit for PhysicsEngine class, view header for more details. */

#include "PhysicsEngine.hpp"

void
PhysicsEngine::movePlatform(lab::Camera &camera, lab::Camera &fps_cam, lab::Mesh* obj, lab::Mesh* player, lab::BlackBox BLACKBOX, bool FPS) {
	glm::mat4 objectModelMatrix = obj->previousModelMatrix;
	// used to compute the movement speed
	float fps = BLACKBOX.getFrameTimeSeconds();
	if (fps == 0.0f) {
		objectModelMatrix = obj->previousModelMatrix;
	}
	else {
		// compute the actual movement speed (not affected by computer speed)
		float actualSpeed = fps * obj->behaviour.speed;
		if (fps <= 1) {
			// compute the translation on all 3 axes 
			glm::vec3 translationVector = obj->behaviour.dir * 
					(actualSpeed * obj->behaviour.increment);
			// if the player is standing on the platform, move him too
			if (onPlatform == obj) {
				glm::vec3 playerTranslation = translationVector;
				player->zfar += playerTranslation.z;
				player->znear += playerTranslation.z;
				player->top += playerTranslation.y;
				player->bottom += playerTranslation.y;
				player->left += playerTranslation.x;
				player->right += playerTranslation.x;
				// update the player model matrix
				glm::mat4 newPlayerModelMatrix = glm::translate(player->
					previousModelMatrix, playerTranslation);
				if (!FPS) {
					// if the camera is not in first person, draw the player
					BLACKBOX.setModelMatrix(newPlayerModelMatrix);
					BLACKBOX.drawMesh(player);
				}
				// update the model matrix
				player->previousModelMatrix = newPlayerModelMatrix;
				// move the cameras with the player
				camera.translateUpword(translationVector.y);
				if (player_dir.x > 0) { 
					camera.translateRight(translationVector.x); 
					fps_cam.translateForward(translationVector.x);
				}
				else if (player_dir.x < 0){
					camera.translateRight(-translationVector.x);
					fps_cam.translateForward(-translationVector.x);
				}
				if (player_dir.z > 0) {
					camera.translateRight(+translationVector.z);
					fps_cam.translateForward(translationVector.z);
				}
				else if (player_dir.z < 0) {
					camera.translateRight(-translationVector.z);
					fps_cam.translateForward(-translationVector.z);
				}
			}
			// update the platform model matrix and positions
			objectModelMatrix = glm::translate(obj->previousModelMatrix, translationVector);
			obj->zfar += translationVector.z;
			obj->znear += translationVector.z;
			obj->top += translationVector.y;
			obj->bottom += translationVector.y;
			obj->left += translationVector.x;
			obj->right += translationVector.x;
			// update the distance traveled
			obj->behaviour.distanceTraveled += abs(translationVector.x) + 
				abs(translationVector.y) + abs(translationVector.z);
		}
	}
	// draw the platform after it has moved
	BLACKBOX.setModelMatrix(objectModelMatrix);
	BLACKBOX.drawMesh(obj);
	obj->previousModelMatrix = objectModelMatrix;
	// reverse direction after if the platform has traveled a suficient distance
	if (obj->behaviour.distanceTraveled >= obj->behaviour.maxDistanceTraveled) {
		obj->behaviour.increment *= -1;
		obj->behaviour.distanceTraveled = 0.0f;
	}
}

void 
PhysicsEngine::movePlayer(lab::Camera &camera, lab::Camera &fps_cam,
						  std::vector<lab::Mesh*> map_objects, lab::Mesh* player, 
						  int direction, lab::BlackBox BLACKBOX, bool FPS) {
	glm::mat4 objectModelMatrix = player->previousModelMatrix;
	// attempt to move player
	float fps = BLACKBOX.getFrameTimeSeconds();
	if (fps == 0) {
		objectModelMatrix = player->previousModelMatrix;
	}
	else {
		// compute actual speed
		float actual_speed = fps * player_speed;
		if (fps < 1) {
			// compute translation on all 3 axes
			glm::vec3 translationVector = player_dir *
				(direction * actual_speed);
			float old_top = player->top, old_bottom = player->bottom;
			float old_left = player->left, old_right = player->right;
			float old_znear = player->znear, old_zfar = player->zfar;
			player->left += translationVector.x;
			player->right += translationVector.x;
			player->znear += translationVector.z;
			player->zfar += translationVector.z;
			// lift a little to avoid collision with platform
			player->top += 0.01f, player->bottom += 0.01f;
			if (lookForCollision(map_objects, player) == NULL) {
				// if the player isn't colliding with anything, move him
				objectModelMatrix = glm::translate(objectModelMatrix, translationVector);
				// update cameras
				if (player_dir.x > 0) camera.translateRight(translationVector.x);
				else camera.translateRight(-translationVector.x);
				if (player_dir.z > 0) camera.translateRight(+translationVector.z);
				else camera.translateRight(-translationVector.z);
			}
			else {
				// revoke movement, return to previous values
				player->znear = old_znear, player->zfar = old_zfar;
				player->left = old_left, player->right = old_right;
			}
			player->top = old_top, player->bottom = old_bottom;
		}
	}
	if (!FPS) {
		// don't draw the player if the camera is FPS
		BLACKBOX.setModelMatrix(objectModelMatrix);
		BLACKBOX.drawMesh(player);
	}
	player->previousModelMatrix = objectModelMatrix;
}

void PhysicsEngine::jumpPlayer() {
	// if the player isn't standing on anything, he can't jump
	if (onPlatform == NULL) return;
	// apply upward momentum
	falling_speed = jump_momentum;
	onPlatform = false;
}

void PhysicsEngine::applyGravity(lab::Camera &camera, lab::Camera &fps_cam, std::vector<lab::Mesh*> map_objects, lab::Mesh* player, lab::BlackBox BLACKBOX, bool FPS) {
	// draw the player
	glm::mat4 objectModelMatrix = player->previousModelMatrix;
	// attempt to apply gravitational force to the player
	float fps = BLACKBOX.getFrameTimeSeconds();
	if (fps == 0) {
		objectModelMatrix = player->previousModelMatrix;
	}
	else {
		// compute actual movement speed
		float actual_descent = fps * falling_speed;
		if (fps < 1) {
			// compute translation on all 3 axes
			glm::vec3 translationVector = glm::vec3(0, -1, 0) *
				(actual_descent);
			float old_top = player->top, old_bottom = player->bottom;
			player->top += translationVector.y;
			player->bottom += translationVector.y;
			// check if the player has fallen on a platjform
			lab::Mesh* platform = lookForCollision(map_objects, player);
			if (platform == NULL) {
				// if not, lower him on the OY axis
				objectModelMatrix = glm::translate
					(objectModelMatrix, translationVector);
				camera.translateUpword(translationVector.y);
				fps_cam.translateUpword(translationVector.y);
				falling_speed += downward_acceleration;
			}
			else {
				// if the player has fallen on a platform
				// stop his fall
				player->top = old_top, player->bottom = old_bottom;
				falling_speed = g;
				onPlatform = platform;
			}
		}
	}
	if (!FPS) {
		// don't draw the player in FPS
		BLACKBOX.setModelMatrix(objectModelMatrix);
		BLACKBOX.drawMesh(player);
	}
	player->previousModelMatrix = objectModelMatrix;
}

void PhysicsEngine::checkForBonuses(lab::Mesh* bonusGravityPlatform, lab::Mesh* bonusSpeedPlatform) {
	if (onPlatform == bonusGravityPlatform) {
		printf("Power up: superman\n");
		jump_momentum = -300.0f;
	}
	if (onPlatform == bonusSpeedPlatform) {
		printf("Power up: speed\n");
		player_speed = 200.0f;
	}
}

lab::Mesh* 
PhysicsEngine::lookForCollision(vector<lab::Mesh*> map_objects, lab::Mesh* obj)
{
	// check if the object collides with any of the platforms
	for (lab::Mesh* platform : map_objects) {
		if (obj->inCollision(platform)) {
			// return the collision platform
			return platform;
		}
	}
	return NULL;
}