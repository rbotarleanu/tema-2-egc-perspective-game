/*	Botarleanu Robert-Mihai 331CB
 *
 *	Translation unit for the GameWindowHandler class, see header for function descriptions.s
 *
 */
#include "GameWindowHandler.hpp"

GameWindowHandler::GameWindowHandler()
{
		// instantiate helper classes
		shapeFactory = new ShapeFactory();
		physicsEngine = new PhysicsEngine();

		// Load Objects
		map_objects = shapeFactory->createMap(bonusGravityPlatform, bonusSpeedPlatform, BLACKBOX);
		resetPlayer();
		cameraTarget = player;
		
		// initializa all key states to not pressed
		memset(keyState, 0, 256);
		memset(specialKeyState, 0, 256);

		// helper variables for tps_camera
		thirdPersonCamera = false;
		
		// Initialize default projection values
		zNear = 100;
		zFar = -500;
		FoV = 120.0f;
		aspectRatio = 800 / 600.0f;

		// value may be used for updating the projection when reshaping the window
		resetCamera();
	
		// set the distance between the TPS camera and the player
		tps_camera.setDistanceToTarget((player->left + player->right)/2.0f);
		// FPS camera is on the player
		fps_camera.setDistanceToTarget(0);
}

GameWindowHandler::~GameWindowHandler()
{
	map_objects.clear();
	delete player;
}

void GameWindowHandler::notifyBeginFrame() { };

void GameWindowHandler::treatInput()
{
	// move player right on current axis
	if (keyState['d']) {
		physicsEngine->movePlayer(tps_camera, fps_camera, map_objects, player, 1, BLACKBOX, false);
		lastDirection = 1;
		setFPSCameraOnPlayer();
	}
	// move player left on current axis
	if (keyState['a']) {
		physicsEngine->movePlayer(tps_camera, fps_camera, map_objects, player, -1, BLACKBOX, false);
		lastDirection = -1;
		setFPSCameraOnPlayer();
	}
	// jump player
	if (keyState[' ']) { physicsEngine->jumpPlayer(); }
	// reset player position
	if (keyState['r']) { 
		resetPlayer(); 
		resetCamera();
	}
}

// resets the cameras with the current player position
void GameWindowHandler::resetCamera() {
	// recompute dimensions with new player position
	orthoLeft = player->left - 75;
	orthoRight = player->right + 75; 
	orthoBottom = player->bottom - 50;
	orthoTop = player->top + 50;
	
	zNear = 100;
	zFar = -500;

	fps_projectionMatrix = glm::perspective(FoV, aspectRatio, 0.1f, 400.0f);
	tps_projectionMatrix = glm::ortho(orthoLeft, orthoRight, orthoBottom, orthoTop, zNear, zFar);

	tps_camera.set(glm::vec3(0, 0, 1), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	setFPSCameraOnPlayer();
	cameraAngle = 0;
}

void GameWindowHandler::setFPSCameraOnPlayer() {
	// set the camera position to be on the player
	fps_camera.set(glm::vec3((player->left + player->right) / 2, player->top + 5.0f,
		player->znear - 2.4f), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	// check which direction the player last moved 
	switch (cameraAngle) {
		case 0: 
			if(lastDirection == 1) fps_camera.rotateFPS_OY(180.0f * 3.14f / 180); 
			break;
		case 90: 
			if (lastDirection == -1) fps_camera.rotateFPS_OY(-90.0f * 3.14f / 180);
			else fps_camera.rotateFPS_OY(90.0f * 3.14f / 180);
			break;
		case 180: 
			if (lastDirection == 1);
			else fps_camera.rotateFPS_OY(180.0f * 3.14f / 180);
			break;
		case 270: 
			if (lastDirection == 1) fps_camera.rotateFPS_OY(-90.0f * 3.14f / 180);
			else fps_camera.rotateFPS_OY(90.0f * 3.14f / 180);
			break;
	}
}

void GameWindowHandler::notifyKeyPressed(unsigned char key_pressed, int mouse_x, int mouse_y)
{
	keyState[key_pressed] = 1;

	if (key_pressed == KEY_ESCAPE)
	{
		glutLeaveMainLoop();
	}


	if (key_pressed == 't') alwaysDrawTarget = !alwaysDrawTarget;

	// Reset Camera
	if (keyState['c']) resetCamera();
}

void GameWindowHandler::notifyKeyReleased(unsigned char key_released, int mouse_x, int mouse_y)
{
	keyState[key_released] = 0;

	// Disable drawing of tps_camera target if no longer moving in ThirdPerson
	if (thirdPersonCamera) {
		char sum = 0;
		for (unsigned int i = 0; i <= 9; i++) {
			sum += keyState[i + '0'];
		}
		if (!sum) thirdPersonCamera = false;
	}
}

void GameWindowHandler::notifySpecialKeyPressed(int key_pressed, int mouse_x, int mouse_y)
{
	specialKeyState[key_pressed] = 1;

	switch (key_pressed) {
	case GLUT_KEY_LEFT: {
		// rotate the TPS camera 90 degrees ccw
        crtRotation -= 90.0f;
		if (cameraAngle + 90 >= 360) cameraAngle = (cameraAngle + 90) % 360;
		else cameraAngle = cameraAngle + 90;
		updatePlayerDirection();
		break;
	}
	case GLUT_KEY_RIGHT: 
		// rotate the TPS camera 90 degrees cw
		crtRotation += 90.0f;
		if (cameraAngle - 90 < 0) cameraAngle = 360 + (cameraAngle - 90);
		else cameraAngle = cameraAngle - 90;
		updatePlayerDirection();
	break;

	default: break;
	}
}

// updates the player movement direction
void GameWindowHandler::updatePlayerDirection() {
	player_dir = glm::vec3(0, 0, 1);
	// check current camera angle on the trygonometric circle to determine
	// axis of movement
	switch (cameraAngle) {
		case 0: player_dir = glm::vec3(1, 0, 0); break;
		case 90: player_dir = glm::vec3(0, 0, -1); break;
		case 180: player_dir = glm::vec3(-1, 0, 0); break;
		case 270: player_dir = glm::vec3(0, 0, 1); break;
	}
	printf("Player direction is now: (%f, %f, %f)\n", player_dir.x, player_dir.y, player_dir.z);
	// notify the physics engine of movement direction change
	physicsEngine->changePlayerDirection(player_dir);
}

void GameWindowHandler::notifySpecialKeyReleased(int key_released, int mouse_x, int mouse_y)
{
	specialKeyState[key_released] = 0;
}

void GameWindowHandler::notifyDisplayFrame()
{
	// Treat continuous input
	treatInput();
	// Clear Color Buffer with the specified color
	glClearColor(1, 1, 1, 0);
	glClear(GL_COLOR_BUFFER_BIT);
	BLACKBOX.notifyDisplay();

	// ----------------------------------
	// Set the viewport and view and projection matrices for the TPS camera
	
	unsigned int width = lab::glut::getInitialWindowInformation().width;
	unsigned int height = lab::glut::getInitialWindowInformation().height;

	// the TPS viewport takes the whole screen
	glViewport(0, 0, width, height);
	// check if we are in the middle of an animation
	if (crtRotation > 0) {
		tps_camera.rotateTPS_OY(TURN_SPEED * 3.14f / 180); 
		crtRotation -= TURN_SPEED;
	}
	if (crtRotation < 0) {
		tps_camera.rotateTPS_OY(-TURN_SPEED * 3.14f / 180); 
		crtRotation += TURN_SPEED;
	}
	// Send view matrix to the GPU
	BLACKBOX.setViewMatrix(tps_camera.getViewMatrix());

	// Send projection matrix to the GPU
	BLACKBOX.setProjectionMatrix(tps_projectionMatrix);

	// draw the platforms
	drawMap(tps_camera, false);

	// apply gravitational force
	physicsEngine->applyGravity(tps_camera, fps_camera, map_objects, player, BLACKBOX, false);

	// check for speed bonuses
	physicsEngine->checkForBonuses(bonusGravityPlatform, bonusSpeedPlatform);

	// set the fps camera position on player
	fps_camera.setPosition(glm::vec3((player->left + player->right) / 2, player->top + 5.0f,
		player->znear - 2.4f));

	// Now draw the map for the FPS camera in a new viewport
	// player fell off the map
	if (player->bottom < -500) { std::cout << "DEAD\n"; resetPlayer(); }

	// viewport for the fps camera
	glViewport(width / 2 + width / 7, height / 2 + height / 8, width / 3, height / 3);
	glEnable(GL_SCISSOR_TEST);
	glScissor(width/2 + width/7, height/2 + height/8, width/3, height/3);
	// clear the buffer, so that there is no overlap between the 2 cameras
	glClear(GL_COLOR_BUFFER_BIT);
	// use the new view and projection matrices
	BLACKBOX.setViewMatrix(fps_camera.getViewMatrix());
	BLACKBOX.setProjectionMatrix(fps_projectionMatrix);
	drawMap(fps_camera, true);
	glDisable(GL_SCISSOR_TEST);
	

	BLACKBOX.notifyDisplay();

}

// draw all platforms
void GameWindowHandler::drawMap(lab::Camera &camera, bool fps) {
	for (lab::Mesh* obj : map_objects) {
		// check if the platform is static
		if (!obj->behaviour.inMovement) {
			glm::mat4 objectModelMatrix = glm::mat4(1.0f);
			BLACKBOX.setModelMatrix(objectModelMatrix);
			BLACKBOX.drawMesh(obj);
		}
		else {
			// if it is mobile, send it to the physics engine for computation
			physicsEngine->movePlatform(camera, fps_camera, obj, player, BLACKBOX, fps);
		}
	}
}

void GameWindowHandler::notifyEndFrame() { }

void GameWindowHandler::notifyReshape(int width, int height, int previos_width, int previous_height)
{
	//blackbox needs to know
	BLACKBOX.notifyReshape(width, height);
	aspectRatio = (float)width / height;
	
}

// resets the player to the original position
void GameWindowHandler::resetPlayer() {
	// original movement direction
	player_dir = glm::vec3(1, 0, 0);
	// send the new direction to the physics engine
	physicsEngine->changePlayerDirection(player_dir);
	// rebind the cube
	player = shapeFactory->createCuboid(BLACKBOX,
		player_coords,
		player_dims.x, player_dims.y, player_dims.z,
		glm::vec3(0, 0, 1));
	// reset the cameras to be centered on the player
	resetCamera();
}

void GameWindowHandler::notifyMouseDrag(int mouse_x, int mouse_y) { }
void GameWindowHandler::notifyMouseMove(int mouse_x, int mouse_y) { }
void GameWindowHandler::notifyMouseClick(int button, int state, int mouse_x, int mouse_y) { }
void GameWindowHandler::notifyMouseScroll(int wheel, int direction, int mouse_x, int mouse_y) { }

