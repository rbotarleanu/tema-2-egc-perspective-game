/*	Botarleanu Robert-Mihai 331CB
 *
 *	Header for a class that provides interfaces for the creation of geometric objects
 *  or geometric compounds to be used in the map.
 *	All created objects are of mesh type.
 */
#pragma once
#include "lab_blackbox.hpp"
#include <vector>

class ShapeFactory {
private:
	// constants for moving platforms
	const float PLATFORM_MOVEMENT_SPEED = 20.0f;
	const float PLATFORM_MAX_DISTANCE_TRAVELED = 50;
public:
	/*********************** Shape creators *************************************/
	/* creates a cube with edge length "l" at the given position*/
	lab::Mesh* const createCube(lab::BlackBox BLACKBOX, glm::vec3 p, 
							    float l, glm::vec3 col);
	/* creates a cuboid with the given height, height and length */
	lab::Mesh* const createCuboid(lab::BlackBox BLACKBOX, glm::vec3 p, 
								  float w, float h, float l, glm::vec3 col);
	/* populates the map with the platforms */
	vector<lab::Mesh*> const createMap(lab::Mesh* &bonusGravityPlatform, lab::Mesh* &bonusSpeedPlatform, 
		lab::BlackBox BLACKBOX);
};
