/*	Botarleanu Robert-Mihai 331CB
*
*	Class which handles all object movement in the level. 
*/
#pragma once

#include "lab_blackbox.hpp"
#include "lab_glut.hpp"
#include "lab_camera.hpp"

#include <vector>

class PhysicsEngine {
private: 
	// direction vector for player
	glm::vec3 player_dir = glm::vec3(1, 0, 0);

	// gravitational acceleration constant
	const float g = 20.0f;
	// falling speed
	float falling_speed = 50.0f;
	// player speed
	float player_speed = 100.0f;
	// platform the player is standing on
	lab::Mesh* onPlatform;
	// downward acceleration that is applied to the gravitational acceleration constant
	// to get the falling speed
	const float downward_acceleration = 0.5f;
	// jump momentum
	float jump_momentum = -150.0f;
private: 
	// finds the platform with which the object is in collision with
	lab::Mesh* lookForCollision(vector<lab::Mesh*> map_objects, lab::Mesh* obj);
public:
	// handles the movement of a platform in an update frame
	void movePlatform(lab::Camera &camera, lab::Camera &fps_cam, lab::Mesh* obj, lab::Mesh* player, lab::BlackBox BLACKBOX, bool FPS);
	
	// handles the movement of a player in an update frame
	void movePlayer(lab::Camera &camera, lab::Camera &fps_cam, std::vector<lab::Mesh*> map_objects, lab::Mesh* player, int direction, lab::BlackBox BLACKBOX, bool FPS);
	// sets the jump momentum
	void jumpPlayer();

	// applies gravitational force on the player
	void applyGravity(lab::Camera &camera, lab::Camera &fps_cam, std::vector<lab::Mesh*> map_objects, lab::Mesh* player, lab::BlackBox BLACKBOX, bool FPS);
	// changes the player's direction
	void changePlayerDirection(glm::vec3 new_direction) {
		player_dir = new_direction;
	}
	// checks for speed and gravity bonus platforms
	void PhysicsEngine::checkForBonuses(lab::Mesh* bonusGravityPlatform, lab::Mesh* bonusSpeedPlatform);
};

