// Translation Unit for ShapeFactory class, see header for details
#include "ShapeFactory.hpp"

lab::Mesh* const
ShapeFactory::createCube(lab::BlackBox BLACKBOX, glm::vec3 p, float l, glm::vec3 col) {
	std::vector<glm::vec3> vertecsi;
	std::vector<unsigned int> indecsi;
	float x = p.r, y = p.g, z = p.b;
	float lat = l / 2;

	//toti vertecsii cubului
	vertecsi.clear();
	vertecsi.push_back(glm::vec3(x - lat, y - lat, z + lat));//0
	vertecsi.push_back(glm::vec3(x + lat, y - lat, z + lat));//1
	vertecsi.push_back(glm::vec3(x + lat, y + lat, z + lat));//2
	vertecsi.push_back(glm::vec3(x - lat, y + lat, z + lat));//3
	vertecsi.push_back(glm::vec3(x - lat, y - lat, z - lat));//4
	vertecsi.push_back(glm::vec3(x + lat, y - lat, z - lat));//5
	vertecsi.push_back(glm::vec3(x + lat, y + lat, z - lat));//6
	vertecsi.push_back(glm::vec3(x - lat, y + lat, z - lat));//7

	//cele 12 laturi ale cubului
	indecsi.push_back(0); indecsi.push_back(1); indecsi.push_back(2);
	indecsi.push_back(0); indecsi.push_back(2); indecsi.push_back(3);
	indecsi.push_back(0); indecsi.push_back(4); indecsi.push_back(7);
	indecsi.push_back(0); indecsi.push_back(7); indecsi.push_back(3);
	indecsi.push_back(0); indecsi.push_back(1); indecsi.push_back(4);
	indecsi.push_back(1); indecsi.push_back(4); indecsi.push_back(5);
	indecsi.push_back(7); indecsi.push_back(3); indecsi.push_back(6);
	indecsi.push_back(3); indecsi.push_back(2); indecsi.push_back(6);
	indecsi.push_back(1); indecsi.push_back(5); indecsi.push_back(6);
	indecsi.push_back(1); indecsi.push_back(6); indecsi.push_back(2);
	indecsi.push_back(5); indecsi.push_back(4); indecsi.push_back(7);
	indecsi.push_back(5); indecsi.push_back(6); indecsi.push_back(7);
	unsigned int segments_vbo, segments_vao, segments_ibo;
	//creaza vao
	glGenVertexArrays(1, &segments_vao);
	glBindVertexArray(segments_vao);

	//creeaza vbo
	glGenBuffers(1, &segments_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, segments_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*vertecsi.size(), 
				 &vertecsi[0], GL_STATIC_DRAW);

	//creeaza ibo
	glGenBuffers(1, &segments_ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, segments_ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int)*indecsi.size(), 
				 &indecsi[0], GL_STATIC_DRAW);

	//creez object de tip segments
	lab::Mesh* mesh = new lab::Mesh(segments_vbo, segments_ibo, segments_vao, indecsi.size());

	// traducem la OpenGL cum sa foloseasca datele noastre
	BLACKBOX.bindMesh(mesh);
	// set colors
	mesh->setColor(col.r, col.g, col.b);
	mesh->setDimensions(z + lat, z - lat, x - lat, x + lat, y - lat, y + lat);

	// return object
	return mesh;
}

lab::Mesh* const
ShapeFactory::createCuboid(lab::BlackBox BLACKBOX, glm::vec3 p, 
						   float w, float h, float l, glm::vec3 col) {
	std::vector<glm::vec3> vertecsi;
	std::vector<unsigned int> indecsi;
	float x = p.r, y = p.g, z = p.b;
	float lat = w / 2;
	float hgt = h / 2;
	float len = l / 2;

	//toti vertecsii cubului
	vertecsi.clear();
	vertecsi.push_back(glm::vec3(x - len, y - lat, z + hgt));//0
	vertecsi.push_back(glm::vec3(x + len, y - lat, z + hgt));//1
	vertecsi.push_back(glm::vec3(x + len, y + lat, z + hgt));//2
	vertecsi.push_back(glm::vec3(x - len, y + lat, z + hgt));//3
	vertecsi.push_back(glm::vec3(x - len, y - lat, z - hgt));//4
	vertecsi.push_back(glm::vec3(x + len, y - lat, z - hgt));//5
	vertecsi.push_back(glm::vec3(x + len, y + lat, z - hgt));//6
	vertecsi.push_back(glm::vec3(x - len, y + lat, z - hgt));//7

	//cele 12 laturi ale cubului
	indecsi.push_back(0); indecsi.push_back(1); indecsi.push_back(2);
	indecsi.push_back(0); indecsi.push_back(2); indecsi.push_back(3);
	indecsi.push_back(0); indecsi.push_back(4); indecsi.push_back(7);
	indecsi.push_back(0); indecsi.push_back(7); indecsi.push_back(3);
	indecsi.push_back(0); indecsi.push_back(1); indecsi.push_back(4);
	indecsi.push_back(1); indecsi.push_back(4); indecsi.push_back(5);
	indecsi.push_back(7); indecsi.push_back(3); indecsi.push_back(6);
	indecsi.push_back(3); indecsi.push_back(2); indecsi.push_back(6);
	indecsi.push_back(1); indecsi.push_back(5); indecsi.push_back(6);
	indecsi.push_back(1); indecsi.push_back(6); indecsi.push_back(2);
	indecsi.push_back(5); indecsi.push_back(4); indecsi.push_back(7);
	indecsi.push_back(5); indecsi.push_back(6); indecsi.push_back(7);
	unsigned int segments_vbo, segments_vao, segments_ibo;
	//creaza vao
	glGenVertexArrays(1, &segments_vao);
	glBindVertexArray(segments_vao);

	//creeaza vbo
	glGenBuffers(1, &segments_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, segments_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*vertecsi.size(),
		&vertecsi[0], GL_STATIC_DRAW);

	//creeaza ibo
	glGenBuffers(1, &segments_ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, segments_ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int)*indecsi.size(),
		&indecsi[0], GL_STATIC_DRAW);

	//creez object de tip segments
	lab::Mesh* mesh = new lab::Mesh(segments_vbo, segments_ibo, segments_vao, indecsi.size());

	// traducem la OpenGL cum sa foloseasca datele noastre
	BLACKBOX.bindMesh(mesh);
	// set colors
	mesh->setColor(col.r, col.g, col.b);
	// set dimensions
	mesh->setDimensions(z + hgt, z - hgt, x - len, x + len, y - lat, y + lat);

	// return object
	return mesh;
}

vector<lab::Mesh*> const 
ShapeFactory::createMap(lab::Mesh* &bonusGravityPlatform, lab::Mesh* &bonusSpeedPlatform, lab::BlackBox BLACKBOX) {
	vector<lab::Mesh*> map;
	// horizontal zone with 10 platforms
	glm::vec3 stcoord(40, 0, 0);
	for (int i = 0; i < 10; ++i) {
		lab::Mesh* platform = createCube(BLACKBOX, stcoord, 20, glm::vec3(0, 1, 0));
		if (i == 4) {
			 platform->setColor(1.0f, 0.30f, 0.65f);
			bonusSpeedPlatform = platform;
		 }
		 map.push_back(platform);
		 stcoord.r += 20;
	}
	// 3 stairs
	stcoord.x -= 20, stcoord.z -= 20;
	for (int i = 0; i < 3; ++i) {
		map.push_back(createCube(BLACKBOX, stcoord, 20, glm::vec3(0, 1, 0)));
		stcoord.y += 15, stcoord.z -= 20;
	}
	// more stairs to the right
	stcoord.z += 20, stcoord.x += 20;
	for (int i = 0; i < 3; ++i) {
		map.push_back(createCube(BLACKBOX, stcoord, 20, glm::vec3(0, 1, 0)));
		stcoord.y += 15, stcoord.x += 20;
	}
	// drop and return to the horizontal 
	stcoord.y -= 40, stcoord.z -= 20;
	for (int i = 0; i < 4; ++i) {
		lab::Mesh* platform = createCube(BLACKBOX, stcoord, 20, glm::vec3(0, 1, 0));
		if (i == 0) {
			platform->setColor(0.98f, 1.0f, 0.40f);
			bonusGravityPlatform = platform;
		}
		map.push_back(platform);
		stcoord.y -= 15, stcoord.z += 20;
	}
	// continues the path, these platforms move on OZ
	for (int i = 0; i < 10; ++i) {
		lab::Mesh* platform = createCube(BLACKBOX, stcoord, 20, glm::vec3(0, 1, 0));
		if (i > 0) {
			platform->behaviour = lab::ObjectBehaviour(true,
				glm::vec3(0, 0, 1 * (i%2==0)?1:-1), PLATFORM_MOVEMENT_SPEED, 
								PLATFORM_MAX_DISTANCE_TRAVELED);
		}
		map.push_back(platform);
		stcoord.r += 20;
	}
	// goes to the horizon now
	for (int i = 0; i < 5; ++i) {
		lab::Mesh* platform = createCube(BLACKBOX, stcoord, 20, glm::vec3(0, 1, 0));
		map.push_back(platform);
		stcoord.z -= 20;
	}
	// drops towards the checkpoint
	stcoord.z += 20, stcoord.x += 20, stcoord.y -= 15;
	for (int i = 0; i < 5; ++i) {
		lab::Mesh* platform = createCube(BLACKBOX, stcoord, 20, glm::vec3(0, 1, 0));
		if (i > 0 && i < 4) {
			platform->behaviour = lab::ObjectBehaviour(true,
				glm::vec3(0, 1 * (i % 2 == 0) ? 1 : -1, 0),
				PLATFORM_MOVEMENT_SPEED, PLATFORM_MAX_DISTANCE_TRAVELED);
		}
		map.push_back(platform);
//		stcoord.x += 20, stcoord.z += 10, stcoord.y -= 15;
		stcoord.x += 20, stcoord.y -= 15;
	}
	// checkpoint
	map.push_back(createCube(BLACKBOX, stcoord, 20, glm::vec3(0.65, 0, 0.82)));
	// return the constructed map
	return map;
}
