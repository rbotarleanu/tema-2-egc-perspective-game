/*	Botarleanu Robert-Mihai 331CB
 *
 *	Class which handles the game window. 
 *	Controls the cameras and draws objects on the screen.
 */
#pragma once
#include "lab_blackbox.hpp"
#include "lab_camera.hpp"
#include "ShapeFactory.hpp"
#include "PhysicsEngine.hpp"
#include <vector>
#include <fstream>

//time
#include <ctime>

#define KEY_ESCAPE					27

class GameWindowHandler : public lab::glut::WindowListener {
private: 
	// The BlackBox hides some functionality that we'll learn in the next course
	lab::BlackBox BLACKBOX;

	// Buffers used for holding state of keys
	// true - pressed
	// false - not pressed
	bool keyState[256];
	bool specialKeyState[256];

	// camera
	lab::Mesh *cameraTarget;

	// helper variables for third person camera drawing
	bool thirdPersonCamera;
	bool alwaysDrawTarget;

	// Projection matrix
	bool isPerspectiveProjection;
	glm::mat4 tps_projectionMatrix;
	glm::mat4 fps_projectionMatrix;
	float FoV;
	float zNear, zFar;
	float aspectRatio;
	float orthoLeft = 0, orthoRight = 0, orthoTop = 0, orthoBottom = 0;

	// Cameras
	lab::Camera tps_camera;
	lab::Camera fps_camera;

	// shape factory, creates all objects on the map
	ShapeFactory* shapeFactory;
	// physics engine, handles all movement
	PhysicsEngine* physicsEngine;

	// objects on screen
	std::vector<lab::Mesh*> map_objects;

	// bonus gravity platform
	lab::Mesh* bonusGravityPlatform;
	// bonus speed platform
	lab::Mesh* bonusSpeedPlatform;

	// player 
	lab::Mesh* player;
	// player dimensions
	glm::vec3 const player_dims = glm::vec3(7.6, 5.0, 6);
	// player start coordinates
	glm::vec3 const player_coords = glm::vec3(2 * 20 * 1, 10 + player_dims.y, 0);

	// bookkeeping of the current rotation angle and player direction
	int cameraAngle = 0;
	glm::vec3 player_dir = glm::vec3(1,0,0);
	
	// rotation that must be done to the ortographic camera
	float crtRotation = 0.0f;

	// current fps camera rotation on OY axis
	float crtFPSrotation = 0.0f;

	// wether the player moved forward or backward on plane
	int lastDirection = 1;

	// controls the turning speed of the animation
	const float TURN_SPEED = 0.175f;

public:
	GameWindowHandler();
	~GameWindowHandler();
private:
	//---------------------------------------------------------------------
	// Loop Functions - function that are called every single frame

	// Called right before frame update callback (notifyDisplayFrame)
	void notifyBeginFrame();

	// Called every frame before we draw
	// Because glut sends only 1 key event every single frame, pressing more than 1 key will have no effect
	// If we treat the input using the 2 special buffers where we hold key states we can treat more than 1
	// key pressed at the same time. Also by not relling on glut to send the continuous pressing event
	// the moving motion will be more smooth because every single frame we apply the changes is contrast to
	// the event based method provided by Glut
	void treatInput();
	// A key was pressed
	void notifyKeyPressed(unsigned char key_pressed, int mouse_x, int mouse_y);
	// When a key was released
	void notifyKeyReleased(unsigned char key_released, int mouse_x, int mouse_y);
	// Special key pressed like the navigation arrows or function keys F1, F2, ...
	void notifySpecialKeyPressed(int key_pressed, int mouse_x, int mouse_y);
	// Called when a special key was released
	void notifySpecialKeyReleased(int key_released, int mouse_x, int mouse_y);
	// Called every frame to draw
	void notifyDisplayFrame();
	// Called when the frame ended
	void notifyEndFrame();

	//---------------------------------------------------------------------
	// Function called when the windows was resized
	// Function called when the windows was resized
	void notifyReshape(int width, int height, int previos_width, int previous_height);
	//---------------------------------------------------------------------
	// Input function

	// Mouse drag, mouse button pressed 
	void notifyMouseDrag(int mouse_x, int mouse_y);

	// Mouse move without pressing any button
	void notifyMouseMove(int mouse_x, int mouse_y);

	// Mouse button click
	void notifyMouseClick(int button, int state, int mouse_x, int mouse_y);

	// Mouse scrolling
	void notifyMouseScroll(int wheel, int direction, int mouse_x, int mouse_y);

	// check for object collisions for player; returns the object with which the player collided
	lab::Mesh* const lookForCollision(lab::Mesh* obj);

	// draws the map platforms
	void drawMap(lab::Camera &camera, bool fps);

	// resets the player
	void resetPlayer();

	// resets the camera
	void resetCamera();

	// updates the player walking direction
	void updatePlayerDirection();

	// sets the FPS camera on top of player
	void setFPSCameraOnPlayer();
};
