// BOTARLEANU ROBERT-MIHAI 331CB
#include "lab_blackbox.hpp"
#include "lab_camera.hpp"
#include "GameWindowHandler.hpp"

int main()
{
	// Initialize GLUT: window + input + OpenGL context
	lab::glut::WindowInfo window(std::string("Tema2 - Botarleanu Robert-Mihai 331CB"), 800, 600, 600, 100, true);
	lab::glut::ContextInfo context(3, 3, false);
	lab::glut::FramebufferInfo framebuffer(true, true, false, false);
	lab::glut::init(window, context, framebuffer);

	// Initialize GLEW + load OpenGL extensions 
	glewExperimental = true;
	glewInit();
	std::cout << "[GLEW] : initializare" << std::endl;

	// Create a new instance of Lab and listen for OpenGL callback
	// Must be created after GLEW because we need OpenGL extensions to be loaded

	GameWindowHandler *gameWindowHandler = new GameWindowHandler();
	lab::glut::setListener(gameWindowHandler);

	// Enter loop
	lab::glut::run();
	
	return 0;
}