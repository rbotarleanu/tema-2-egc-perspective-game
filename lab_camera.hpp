//-------------------------------------------------------------------------------------------------
// Descriere: header camera
//
// Autor: Lucian Petrescu
// Data: 14 oct 2013
//-------------------------------------------------------------------------------------------------

#define _USE_MATH_DEFINES
#include <math.h>

#pragma once
#include "dependente\glm\glm.hpp"
#include "dependente\glm\gtc\type_ptr.hpp"
#include "dependente\glm\gtc\matrix_transform.hpp"

#define PI 3.14159265358979323846

static glm::vec3 WORLD_UP = glm::vec3(0, 1, 0);

// Rotate a Point/Vector around world OY (0, 1, 0) with a specific angle(radians)
inline glm::vec3 RotateOY(const glm::vec3 P, float radians)
{
	glm::vec3 R;
	R.x = P.x * cos(radians) - P.z * sin(radians);
	R.y = P.y;
	R.z = P.x * sin(radians) + P.z * cos(radians);
	return R;
}

namespace lab
{
	class Camera
	{
	public:
		Camera()
		{
			position = glm::vec3(0, 0, 0);
			forward = glm::vec3(0, 0, -1);
			up		= glm::vec3(0, 1, 0);
			right	= glm::vec3(1, 0, 0);
			distanceToTarget = 40;
		}

		Camera(glm::vec3 position, glm::vec3 forward, glm::vec3 up, glm::vec3 right, float distanceToTarget) {
			this->position = position;
			this->forward = forward;
			this->up = up;
			this->right = right;
			this->distanceToTarget = distanceToTarget;
		}

		Camera(const glm::vec3 &position, const glm::vec3 &center, const glm::vec3 &up)
		{
			set(position, center,up);
		}

		~Camera()
		{ }

		void setDistanceToTarget(float d) { distanceToTarget = d;  }

		// Update camera
		void set(const glm::vec3 &position, const glm::vec3 &center, const glm::vec3 &up)
		{
			this->position = position;
			forward = glm::normalize(center-position);
			right	= glm::cross(forward, up);
			this->up = glm::cross(right,forward);
		}

		void moveForwardKeepOY(float distance)
		{
			glm::vec3 dir = glm::normalize(glm::vec3(forward.x, 0, forward.z));
			position += glm::normalize(dir) * distance;
		}

		void translateForward(float distance)
		{
			position += distance * glm::normalize(forward);
		}

		void translateUpword(float distance)
		{
			position += distance * glm::normalize(up);
		}

		void translateRight(float distance)
		{
			glm::vec3 dir = glm::normalize(glm::vec3(right.x, 0, right.z));
			position += dir * distance;
		}

		// rotates around the OY axis in first person
		void rotateFPS_OY(float angle)
		{
			forward = RotateOY(glm::normalize(forward), angle);
			right = RotateOY(glm::normalize(right), angle);
			up = glm::cross(right, forward);
		}

		// rotates around the OY axis in third person
		void rotateTPS_OY(float angle)
		{
			translateRight(distanceToTarget);
			rotateFPS_OY(angle);
			translateRight(-distanceToTarget);
		}


		glm::mat4 getViewMatrix()
		{
			// Return the View Matrix
			return glm::lookAt(position, position + forward, up);
		}

		glm::vec3 getTargetPosition()
		{
			return position + forward * distanceToTarget;
		}

		glm::vec3 getPosition()
		{
			return position;
		}

		glm::vec3 getForward()
		{
			return forward;
		}

		void setPosition(glm::vec3 position) 
		{ 
			this->position = position; 
		}

	private:
		float distanceToTarget;
		glm::vec3 position;
		glm::vec3 forward;
		glm::vec3 right;
		glm::vec3 up;
	};
}